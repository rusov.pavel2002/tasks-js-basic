// Solutions for homework tasks

// Insert Task 1 solution here

const isAnagram = function(s, t) {
    if (s.length != t.length) {
        return false
        return false
    } else {
        for (let i=0; i<s.length; i++) {
            if (t.includes(s[i])) {
                t = t.replace(s[i], '');
                t += '0';
                t = t.replace(s[i], '');
                t += '0';
            } else {
                return false
            }
        }
    }
    
    return true
};

// Insert Task 2 solution here

// когда забыл про Эратосфена:
//  Time Limit Exceeded

const countPrimes = function(n) {
    if (n < 2) {
        return 0
    };
    
    let counter = 0;
    for (let i=n-1; i>1; i--) {
        const half = Math.ceil(i / 2) + 1;
        for (let j=2; j<half; j++) {
            if (i % j === 0) {
                console.log(i)
                counter += 1;
                break;
            }
        }
    }
    
    return n - counter - 2;
};

// когда вспомнил...

const newCountPrimes = function(n) {
    if (n < 2) {
        return 0;
    }
    
    workField = [1 , 1];
    for (let i=2; i<n; i++) {
        workField.push(0);
    }
    const lastDivider = Math.floor(Math.sqrt(n));
    for (let j=2; j<lastDivider+1; j++) {
        let curIndex = j;
        do {
            if (curIndex != j && workField[curIndex] == 0) {
                workField[curIndex] = 1;
            }
            curIndex += j;
        } while (curIndex < n)
    }
    let counter = 0;
    workField.forEach((value, index) => {
        if (value === 0) {
            counter += 1;
        }
    })
    return counter
};



// Insert Task 3 solution here

const romanToInt = function(s) {
    valuesSymbols = {
        'I': 1, 'V': 5, 'X': 10, 'L': 50,'C': 100, 'D': 500, 'M': 1000,
    };
    let amount = 0;
    Array.from(s).map((value, index) => {    
        if (index < s.length - 1 && valuesSymbols[value] < valuesSymbols[s[index + 1]]) {
            amount -= valuesSymbols[value];
        } else {
            amount += valuesSymbols[value];
        }
    })
    return amount
};

// Insert Task 4 solution here
const merge = function(nums1, m, nums2, n) {
    for (let i=nums1.length - 1; i>-1; i--) {
        if (n < 1) {
            break
        } else {
            if (nums1[m-1] > nums2[n-1]) {
                nums1[m + n - 1] = nums1[m-1]
                m--
            } else {
                nums1[m + n - 1] = nums2[n-1]
                n--
            }
        }
    }
};


// Insert Task 5 solution here

var Solution = function (nums) {
	this.nums = [...nums]
	this.arr = nums
}

Solution.prototype.reset = function () {
	this.arr = [...this.nums]
	return this.arr
}

Solution.prototype.shuffle = function() {
    let array = this.arr
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }  
    return array
};
